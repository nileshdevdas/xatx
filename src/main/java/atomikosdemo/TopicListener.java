package atomikosdemo;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class TopicListener {

	public static void main(String[] args) throws Exception {
		ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://localhost:61616");
		Connection connection = cf.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Topic topic = session.createTopic("sapient.t");
		MessageConsumer consumer = session.createConsumer(topic);
		while (true) {
			TextMessage msg = (TextMessage) consumer.receive();
			System.out.println(msg);
		}
		// session.commit();
	}
}
