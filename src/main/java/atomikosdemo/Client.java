package atomikosdemo;

import java.util.Collections;
import java.util.Random;

import org.bson.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Client {

	public static void main(String[] args) throws Exception {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("sapient");
		MongoCollection collection = db.getCollection("Users");

		for (int i = 0; i < 100000; i++) {
			/* Using a jackson api i have converted object to json string */
			User user = new User();
			user.setId(new Random().nextInt());
			user.setName("name " + user.getId());
			user.setEmployee(true);
			user.setRole("XX");
			ObjectMapper mapper = new ObjectMapper();
			Document doc = Document.parse(mapper.writeValueAsString(user));
			collection.insertOne(doc);
		}

	}
}
