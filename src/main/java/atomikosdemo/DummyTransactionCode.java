package atomikosdemo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.XAConnectionFactory;
import javax.transaction.UserTransaction;

import org.apache.activemq.spring.ActiveMQXAConnectionFactory;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.atomikos.jms.AtomikosConnectionFactoryBean;

public class DummyTransactionCode {

	public static void main(String[] args) throws Exception {
		MessageProducer producer = null;
		Session session = null;
		UserTransaction utx = new UserTransactionImp();
		Connection con = null;
		Connection con2 = null;
		XAConnectionFactory xacf = null;
		javax.jms.Connection jmconnection = null;
		try {
			utx.begin();
			con = mysqlInsert();
			con2 = postgresInsert();
			jmsPut();
			utx.setRollbackOnly();
		} catch (Exception e) {
			e.printStackTrace();
			utx.rollback();
		} finally {
			if (con != null && !con.isClosed()) {
				System.out.println("Connection 1 Closing");
				con.close();
			}
			if (con2 != null && !con2.isClosed()) {
				System.out.println("Connection 2 Closing");
				con2.close();
			}
			if (jmconnection != null) {
				System.out.println("Jms Connection Closing ");
				jmconnection.close();
			}
			if (producer != null) {
				producer.close();
			}
			if (session != null) {
				session.close();
			}
		}
	}

	private static void jmsPut() throws JMSException {
		javax.jms.Connection jmconnection = null;
		Session session = null;
		MessageProducer producer = null;
		try {
			XAConnectionFactory xacf = new ActiveMQXAConnectionFactory();
			AtomikosConnectionFactoryBean cf = new AtomikosConnectionFactoryBean();
			cf.setUniqueResourceName("activemq");
			cf.setXaConnectionFactory(xacf);
			jmconnection = cf.createConnection();
			session = jmconnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue q = session.createQueue("transact.q");
			TextMessage tms = session.createTextMessage("if you see this then commited...");
			
			producer = session.createProducer(q);
			producer.send(tms);
		} catch (JMSException e) {
			e.printStackTrace();
		} finally {
			jmconnection.close();
			session.close();
			producer.close();
		}
	}

	private static Connection postgresInsert() throws SQLException {
		Connection con2;
		AtomikosDataSourceBean datSource2 = new AtomikosDataSourceBean();
		Properties config2 = new Properties();
		config2.put("url", "jdbc:postgresql://localhost/postgres");
		config2.put("user", "tmdemo");
		config2.put("password", "tmdemo");
		datSource2.setUniqueResourceName("mypgsql");
		datSource2.setXaDataSourceClassName("org.postgresql.xa.PGXADataSource");
		datSource2.setXaProperties(config2);
		con2 = datSource2.getConnection();
		var st2 = con2.createStatement();
		st2.executeUpdate("insert into tmdemo values('tx1')");
		return con2;
	}

	private static Connection mysqlInsert() throws SQLException {
		Connection con;
		/**** MySQL DataSource + Insert */
		AtomikosDataSourceBean datSource1 = new AtomikosDataSourceBean();
		var config = new Properties();
		config.put("url", "jdbc:mysql://localhost/test");
		config.put("user", "root");
		config.put("password", "");
		datSource1.setXaDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlXADataSource");
		datSource1.setUniqueResourceName("mysqlds1");
		datSource1.setXaProperties(config);
		con = datSource1.getConnection();
		try (var st = con.createStatement()) {
			st.execute("insert into tmtest values('tx1')");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}
}
