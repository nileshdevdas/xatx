package atomikosdemo;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.policies.DefaultRetryPolicy;
import com.datastax.driver.core.policies.RoundRobinPolicy;

public class CassandraDemo {

	public static void main(String[] args) {
		Cluster cluster = Cluster.builder().addContactPoint("localhost:9042")
				.withLoadBalancingPolicy(new RoundRobinPolicy()).withRetryPolicy(DefaultRetryPolicy.INSTANCE).build();
		Session session = cluster.connect("sapient");
		session.execute("select * from users");
	}
}
