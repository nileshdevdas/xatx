package atomikosdemo;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class TopicBroadcast {

	public static void main(String[] args) throws Exception {

		ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://localhost:61616");
		Connection connection = cf.createConnection();
		// i need to select between the desitnation whether queue / whether topic
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Topic queue = session.createTopic("sapient.t");
		TextMessage message = session.createTextMessage("Its a coool Jms Service....");
		System.out.println(message);
		MessageProducer producer = session.createProducer(queue);
		producer.send(message);
		// session.commit();
	}
}
