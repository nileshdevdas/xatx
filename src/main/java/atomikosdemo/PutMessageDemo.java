package atomikosdemo;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class PutMessageDemo {

	public static void main(String[] args) throws Exception {

		ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://localhost:61616");
		Connection connection = cf.createConnection();
		// i need to select between the desitnation whether queue / whether topic
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue("sapient.q");
		TextMessage message = session.createTextMessage("xt1");
		message.setStringProperty("app", "app1");
		message.setStringProperty("version", "1");
		System.out.println(message);
		MessageProducer producer = session.createProducer(queue);
		for (int i = 0; i < 1000; i++) {
			producer.send(message);
		}
		// session.commit();
	}
}
